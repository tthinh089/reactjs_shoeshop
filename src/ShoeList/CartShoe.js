import React, { Component } from "react";

export default class CartShoe extends Component {
  render() {
    let { cart, handleRemove, handleAmount } = this.props;
    return (
      <div className="col-6">
        <table className="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Amount</th>
              <th>Price</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {cart.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{item.name}</td>
                  <td className>
                    <button
                      className="btn btn-primary"
                      onClick={() => {
                        handleAmount(item.id, -1);
                      }}
                    >
                      -
                    </button>
                    <strong className="mx-2">{item.soLuong}</strong>
                    <button
                      className="btn btn-primary"
                      onClick={() => {
                        handleAmount(item.id, +1);
                      }}
                    >
                      +
                    </button>
                  </td>
                  <td>{item.price * item.soLuong}</td>
                  <td>
                    <img
                      src={item.image}
                      style={{ width: 50 }}
                      alt="shoe img"
                    />
                  </td>
                  <td>
                    <button
                      className="btn btn-danger"
                      onClick={() => {
                        handleRemove(item.id);
                      }}
                    >
                      X
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
