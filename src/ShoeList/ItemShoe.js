import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { data, handleWatchDetail, handleBuyShoe } = this.props;
    let { image, name } = this.props.data;
    return (
      <div className="col-6 p-4">
        <div className="card text-left h-100 p-1">
          <img className="card-img-top" src={image} alt="shoe img" />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
          </div>
          <button
            className="btn btn-primary"
            onClick={() => {
              handleWatchDetail(data);
              // this.props.handleWatchDetail(this.props.data);
            }}
          >
            View Detail
          </button>
          <button
            className="btn btn-danger"
            onClick={() => {
              handleBuyShoe(data);
            }}
          >
            Buy Now!
          </button>
        </div>
      </div>
    );
  }
}
